#ifndef MOJ_NIZ
#define MOJ_NIZ value
#include <stddef.h>
#include <initializer_list>
#include <algorithm>
#include <stdexcept>

template<typename T>
class MojNiz{
  private:
    size_t size_;
    size_t cap_;
    T * ptr_;
  public:
    MojNiz() : size_{0}, cap_{1}, ptr_{new T[cap_]} {}

    MojNiz(std::initializer_list<T> init_list) : size_{init_list.size()},
      cap_{size_}, ptr_{new T[cap_]} {
        std::copy(init_list.begin(), init_list.end(), ptr_);
      }

    MojNiz(const MojNiz& other) : size_{other.size()}, cap_{other.capacity()}, 
      ptr_{new T[cap_]}{
        std::copy(other.begin(), other.end(), ptr_);
    }

    template<typename U>
      MojNiz(const MojNiz<U>& other) : size_{other.size()}, cap_{other.capacity()},
      ptr_{new T[cap_]}{
        std::copy(other.begin(), other.end(), ptr_);
      }

    MojNiz(MojNiz&& other) : size_{other.size()}, cap_{other.capacity()}, 
      ptr_{other.begin()} {
    other.ptr_=nullptr;
    other.size_=0;
    other.cap_=0;
    }

    ~MojNiz() {
    delete[] ptr_;
    }

    size_t size() const{return size_;}
    
    size_t capacity() const {return cap_;}

    T* begin() const{return ptr_;}

    T* begin(){return ptr_;}

    T* end() const{return ptr_+size_;}
 
    T* end(){return ptr_+size_;}
 
    T& at(size_t index)const{
      if(index < 0 || index >= size_)
        throw std::out_of_range("Index out of boarders");
      return ptr_[index];
    }

    T& front() const{
      return ptr_[0];
    }

    T& back() const{
      return ptr_[size_-1];
    }

    T& front() {
      return ptr_[0];
    }

    T& back() {
      return ptr_[size_-1];
    }

    void push_back(const T& val){
      if(size_==cap_){
        cap_*=2;
        T * new_ptr = new T [cap_];
        std::copy(ptr_, ptr_+size_, new_ptr);
        delete [] ptr_;
        ptr_=new_ptr;
      }
      ptr_[size_++] = val;
    }

    void pop_back(){
      if(size_ > 0)
        --size_;
    }

    MojNiz& operator = (const MojNiz& other){
      if(this != &other){
        if(size_ > 0)
          delete [] ptr_;
        size_=other.size();
        cap_=other.capacity();
        ptr_ = new T [cap_];
        std::copy(other.begin(), other.end(), ptr_);
      }
      return *this;
    }

    MojNiz& operator = (MojNiz&& other){
      if(size_>0)
        delete [] ptr_;
      size_=other.size();
      cap_=other.capacity();
      ptr_=other.begin();
      other.begin()=nullptr;
      other.size()=0;
      other.capacity()=0;
      return *this;
    }

    template<typename U>
      MojNiz& operator = (const MojNiz<U>& other){
        size_=other.size();
        cap_=other.capacity();
        delete [] ptr_;
        ptr_=new T[cap_];
        std::copy(other.begin(), other.end(), ptr_);
        return *this;
      }

    T& operator[](size_t index){
      if(index < 0 || index >= size_)
        throw std::out_of_range("Index out of borders");
      return ptr_[index];
    }

    T& operator[](size_t index) const{
      if(index < 0 || index >= size_)
        throw std::out_of_range("Index out of borders");
      return ptr_[index];
    }

    MojNiz& operator++(){
      for(int i = 0; i < size_; ++i)
        ++ptr_[i];
      return *this;
    }

    MojNiz operator++(int){
      MojNiz<T> new_array = *this;
      for(size_t i = 0; i < size_; ++i)
        ++ptr_[i];
      return new_array;
    }

    MojNiz operator + (const MojNiz& other) const {
      if(other.size() != size_)
        throw std::invalid_argument("Containers are not the same size");
      MojNiz<T> new_array;
      for(size_t i = 0; i < size_; ++i)
        new_array.push_back(ptr_[i] + other[i]);
      return new_array;
    }

    MojNiz operator * (int scalar) const {
      MojNiz<T> new_array;
      for(size_t i = 0; i < size_; ++i)
        new_array.push_back(scalar * ptr_[i]);
      return new_array;
    }

    template<typename U>
      auto operator + (const MojNiz<U> other) const {
        if(other.size() != size_)
          throw std::invalid_argument("Containers are not the same size");
        MojNiz<decltype(ptr_[0] + other[0])> new_array;
        for(size_t i = 0; i < size_; ++i)
          new_array.push_back(ptr_[i] + other[i]);
        return new_array;
      }
};


#endif /* ifndef MOJ_NIZ */
