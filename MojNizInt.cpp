#include "MojNizInt.hpp"

size_t MojNizInt::size() const{return size_;}
size_t MojNizInt::capacity() const{return cap_;}

int& MojNizInt::at(size_t index) const{
  if(index < 0 || index >= size_ )
    throw std::out_of_range("Index out of borders");
  return ptr_[index];
}

int& MojNizInt::front() const {
  if(size_ != 0)
    return ptr_[0];
  else throw std::out_of_range("Array is empty");
}

int& MojNizInt::back() const{
  if(size_ != 0)
    return ptr_[size_-1];
  else throw std::out_of_range("Array is empty");
}

void MojNizInt::push_back(int val){
  if(size_ == cap_){
    cap_*=2;
    int * new_ptr = new int[cap_];
    std::copy(ptr_, ptr_+size_, new_ptr);
    delete [] ptr_;
    ptr_=new_ptr;
  }
  ptr_[size_++]=val;
}

void MojNizInt::pop_back(){
  if(size_ != 0)
    --size_;
  else throw std::out_of_range("Array is empty");
}

MojNizInt& MojNizInt::operator=(const MojNizInt& other){
  if(&other != this){
    delete[] ptr_;
    size_ = other.size_;
    cap_ = other.cap_;
    ptr_ = new int[cap_];
    std::copy(other.ptr_, other.ptr_ + size_, ptr_);
  }
  return *this;
}

MojNizInt& MojNizInt::operator=(MojNizInt&& other){
  delete[] ptr_;
  ptr_ = other.ptr_;
  size_ = other.size_;
  cap_ = other.cap_;
  other.ptr_ = nullptr;
  other.size_ = 0;
  other.cap_ = 0;
  return *this;
}

const int& MojNizInt::operator[] (int index) const{
  return at(index);
}

int& MojNizInt::operator[] (int index){
  return at(index);
}

MojNizInt MojNizInt::operator * (int scalar) const{
  MojNizInt new_array;
  for(int i = 0; i < size_; ++i)
    new_array.push_back(scalar * ptr_[i]);
  return new_array;
}

MojNizInt MojNizInt::operator+(const MojNizInt& other) const{
  if(other.size_ != size_)
    throw std::invalid_argument("Containers are not the same size");
  MojNizInt new_array;
  for(int i = 0; i < size_; ++i)
    new_array.push_back(ptr_[i] + other.ptr_[i]);
  return new_array;
}

MojNizInt& MojNizInt::operator++(){
  for(int i = 0; i < size_; ++i)
    ++ptr_[i];
  return *this;
}

MojNizInt MojNizInt::operator++(int){
 MojNizInt new_array = *this;
 for(int i = 0; i < size_; ++i)
   ++ptr_[i];
 return new_array;
}


