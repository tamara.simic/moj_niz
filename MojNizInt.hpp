#ifndef MOJNIZINT_H
#define MOJNIZINT_H value
#include <cstddef>
#include <initializer_list>
#include <iostream>
class MojNizInt{
  private:
    size_t size_;
    size_t cap_;
    int * ptr_;
  public:
    MojNizInt() : size_{0}, cap_{1}, ptr_{new int[cap_]} {}

    MojNizInt(std::initializer_list<int> init_list) : size_{init_list.size()},
      cap_{size_}, ptr_{new int[cap_]} {
        std::copy(init_list.begin(), init_list.end(), ptr_);
      }

    MojNizInt(const MojNizInt& other) : size_{other.size_}, cap_{other.cap_}, 
      ptr_{new int[cap_]} {
      std::copy(other.ptr_, other.ptr_ + size_, ptr_);
    }

    MojNizInt(MojNizInt&& other) : size_{other.size_}, cap_{other.cap_}, 
      ptr_{other.ptr_} {
    other.ptr_=nullptr;
    other.size_=0;
    other.cap_=0;
    }

    ~MojNizInt() {
    delete[] ptr_;
    }

    size_t size() const;
    size_t capacity() const;
    int& at(size_t index) const;
    int& front() const;
    int& back() const;
    void push_back(int val);
    void pop_back();

    MojNizInt& operator = (const MojNizInt& other);
    MojNizInt& operator = (MojNizInt&& other);
    const int& operator [] (int index) const;
    int& operator [](int index);
    MojNizInt operator * (int scalar) const; 
    MojNizInt operator + (const MojNizInt& other) const;
    MojNizInt& operator ++ ();
    MojNizInt operator ++ (int);
    
};
#endif /* ifndef MOJNIZINT_H */
